**THIS PROJECT IS STILL INACTIVE. I CREATED THIS REPOSITORY TO REMIND MYSELF TO BUILD THIS
UTILITY ONCE MY EXAMS ARE OVER AND I HAVE SOME TIME.**

# nudge

nudge aims to be a simple bash utility for setting up reminders that will *nudge* you a set
time before the event you want to be reminded of using desktop notifications.

### Dependencies

* `notify-send` from `libnotify`

### Installing

If you want to keep the script up-to-date with this repository:
```
git clone https://gitlab.com/mcramer/nudge.gitlab
chmod +x nudge/nudge
```

If you just want the bash file as-is at time of download:
```
curl https://gitlab.com/mcramer/nudge/-/raw/main/nudge
```
Switch out `curl` for `wget` or any other downloader you might use.

### Usage
#TODO

### License

This project is licensed under the MIT License - see the LICENSE.txt file for details

### Contributing

If you have feedback on the code, want to suggest features or similar, feel free to open
a new [Issue](https://gitlab.com/mcramer/nudge/-/issues/new) or [Merge
Request](https://gitlab.com/mcramer/dotfiles/-/merge_requests/new) and I'll take a look!
Be aware though, this script is mainly [made for
myself](https://blubsblog.bearblog.dev/i-am-the-only-user/), so I might not implement your
suggestion, even if you made a Merge Request.
